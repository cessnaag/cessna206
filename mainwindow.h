#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtWidgets>
#include "math.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    enum ScreenOrientation {
        ScreenOrientationLockPortrait,
        ScreenOrientationLockLandscape,
        ScreenOrientationAuto
    };

    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();
    QAction *exitApp;
    QAction *actionExit;
    QAction *helpApp;
    QPushButton *butRasch;
    QPushButton *butRaschVes;
    QPushButton *pbLbs;
    QPushButton *pbKg;
    QPushButton *pbPress;
    QPushButton *pbH;
    QPushButton *pbU;
    QPushButton *pbMC;
    QSpinBox *spinBoxS;
    QSpinBox *spinTks;
    QSpinBox *spinBoxSalt;
    QSpinBox *spinEk;
    QSpinBox *spinZagr;
    QSpinBox *spinZagr_2;
    QSpinBox *spinZagr_3;
    QSpinBox *spinCargoA;
    QSpinBox *spinCargoB;
    QSpinBox *spinCargoC;
    QSpinBox *spinCargoD;
    QSpinBox *spinBg4;
    QSpinBox *spinBg6;
    QSpinBox *spinBSP;
    QSpinBox *spinBSP1;
    QLabel *label_31;
    QLabel *label_32;
    QLineEdit *lineEditG;
    QLineEdit *lineEditGp;
    QLineEdit *lineEditGz;
    QLineEdit *lineEditG_2;
    QLineEdit *lineVzl;
    QLineEdit *lineCentr;
    QLineEdit *lineCG;
    QLineEdit *lineLbs1;
    QLineEdit *lineKg1;
    QLineEdit *lineLbs2;
    QLineEdit *lineKg2;
    QLineEdit *lineGP;
    QLineEdit *lineMM;
    QLineEdit *linePr;
    QLineEdit *lineHA;
    //QLineEdit *lineMK;
    QLineEdit *lineUV;
    //QLineEdit *lineVet;
    QLineEdit *lineUB;
    QLineEdit *lineU;
    QLineEdit *lineQNH;
    QLineEdit *lineQFE;
    QLineEdit *lineMC;
    QLineEdit *lineKM;
    QComboBox *cbBort;
    QComboBox *comboBrake;
    QComboBox *comboWind;
    //QGraphicsView *Mnemo;
    QAction *exit;
    QCheckBox *cbLeto;




    // Note that this will only have an effect on Symbian and Fremantle.
    void setOrientation(ScreenOrientation orientation);

    void showExpanded();

public slots:
    void fuel();
    void pola(int comboIndex);
    void ves();
    void lbs();
    void kg();
    void mm();
    void closeapp();
    void leto();
    void ha();
    void vet();
    void mc();

private slots:
    void about();
    void help();

signals:
    //void valueChanged(int newValue);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
